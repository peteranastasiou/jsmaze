
// MAZE
// Inspired by the original MAZE WAR graphics
// Originally written in 'droidscript' on a mobile on an aeroplane
// Ported to javascript soon after.

var MW = 15;
var MH = MW;
var BW, BH;
var px, py;
var kx, ky;
var angle = 0;

var EMPTY = 0;
var EXPLORED = -1;
// WALL is any other number 0.1 to 1
var g, canvas;

var N = 0;
var E = 1;
var S = 2;
var W = 3;
var dx = [0, 1, 0, -1];
var dy = [1, 0, -1, 0];

var maxdepth;
var map = new Array(MW * MH); //0 unexplored, 1 wall, -1 explored

function mouseUp(event) {
  //TODO
}

function getMapIdx(x, y) {
    return Math.floor(x + y * MW);
}

function getMap(x, y) {
    if (!inBounds(x, y))
        return 1;
    return map[getMapIdx(x, y)];
}

function setMap(x, y, a) {
    if (inBounds(x, y)) {
        map[getMapIdx(x, y)] = a;
    }
}

function inBounds(x, y) {
    if (x < 0 || y < 0 || x >= MW || y >= MH) {
        return false;
    }
    return true;
}

function maze() {
    canvas = document.getElementById('gameCanvas');
    g = canvas.getContext('2d');
    BW = canvas.width;
    BH = canvas.height;

    newMap();
    draw();
}

function keyDown(event) {
    code = event.keyCode;
    if(37 <= code && code <= 40) {
        event.preventDefault();
    }
    if (code === 37) // left arrow
        left();
    if (code === 38) // up arrow
        forward();
    if (code === 39) // right arrow
        right();
}

function mouseDown(event) {
    event.preventDefault();
}

function mouseUp(event) {
    event.preventDefault();
    var rect = canvas.getBoundingClientRect();
    var tx = event.clientX - rect.left;
    var ty = event.clientY - rect.top;
    if(tx < BW/3.0) {
        left();
    } else if(tx < 2.0*BW/3.0) {
        forward();
    } else {
        right();
    }
}

function right() {
    angle += Math.PI / 2;
    draw();
}

function left() {
    angle -= Math.PI / 2;
    draw();
}

function forward() {
    nx = px + cosi(angle);
    ny = py + sini(angle);
    if (getMap(nx, ny) <= EMPTY) { // i.e. empty or explored
        px = nx;
        py = ny;
    }
    draw();
}

// Place key at max depth
function carveMaze(x0, y0, depth) {
    // list of directions to explore:
    var dirs = [N, E, S, W];
    // try all directions:
    while(dirs.length > 0) {
        // pop random direction remaining:
        var i = randInt(0, dirs.length);
        var dir = dirs[i];
        dirs.splice(i, 1);   // remove from array
        
        // move in that direction twice:
        var x1 = x0 + dx[dir];
        var y1 = y0 + dy[dir];
        var x2 = x1 + dx[dir];
        var y2 = y1 + dy[dir];
        
        // if within bounds:
        if(0 <= x2 && x2 < MW && 0 <= y2 && y2 < MH && getMap(x2,y2) !== EMPTY) {
            // carve two spaces:
            setMap(x1, y1, EMPTY);
            setMap(x2, y2, EMPTY);
            // update key location:
            if(depth > maxdepth) {
                kx = x2;
                ky = y2;
                maxdepth = depth;
            }
            // recurse:
            carveMaze(x2, y2, depth+1);
        }
    }
}

function newMap() {
    map = new Array(MW * MH);
    map.fill(1);
    
    // Use recursive backtracing to make a maze
    // pick a random even number (odds are walls):
    px = 2*randInt(0, MW/2);
    py = 2*randInt(0, MH/2);
    
    maxdepth = 0;
    carveMaze(px, py, 0);
}

//Update and redraw graphics. 
function draw() {
    // Clear:
    g.fillStyle = '#303030';
    g.fillRect(0, 0, BW, BH);
    g.strokeStyle = '#FFFFFF';

    // draw in psuedo 3d:
    var z = 1;
    var x = px;
    var y = py;
    while (getMap(x, y) <= EMPTY) {
        var isRightWall = getMap(x + cosi(angle + Math.PI / 2), y + sini(angle + Math.PI / 2)) > 0;
        var isLeftWall = getMap(x + cosi(angle - Math.PI / 2), y + sini(angle - Math.PI / 2)) > 0;
        var isEnd = getMap(x + cosi(angle), y + sini(angle)) > 0;
        drawRight(tr(z - 1), tr(z), isRightWall, isEnd);
        drawLeft(tr(z - 1), tr(z), isLeftWall, isEnd);
        if (x === kx && y === ky) {
            g.strokeStyle = '#FF00FF';
            drawKey(z);
            g.strokeStyle = '#FFFFFF';
        }
        setMap(x, y, EXPLORED); // explored
        z ++;
        x += cosi(angle);
        y += sini(angle);
    }

    // draw in 2d
    var ox = 0.3 * BW;
    var oy = 0.68 * BH;
    var sz = 0.40 * BW;
    g.fillStyle = '#606060';
    for (var x = 0; x < MW; x++) {
        for (var y = 0; y < MH; y++) {
            if (getMap(x, y) === -1) {
                g.fillStyle = '#606060';
            } else {
                g.fillStyle = '#202020';
            }
            g.fillRect(ox + x * sz / MW, oy + y * sz / MH,
                    0.9 * sz / MW, 0.9 * sz / MH);

        }
    }
    g.fillStyle = '#FFFFFF';
    g.beginPath();
    g.arc(ox + (px + 0.45) * sz / MW, oy + (py + 0.45) * sz / MH,
            0.4 * sz / MH, angle - Math.PI*.7 + Math.PI, angle + Math.PI*.7 + Math.PI);
    g.fill();
    // Reveals key location:
    //canvas.fillStyle = '#FF00FF';
    //canvas.beginPath();
    //canvas.arc(ox + (kx + 0.45) * sz / MW, oy + (ky + 0.45) * sz / MH,
    //        0.4 * sz / MH, 0, 2*Math.PI);
    //canvas.fill();

    if (kx === px && ky === py) {
        alert('COMPLETE');
        newMap();
        draw();
    }
}

function cosi(theta) {
    return Math.round(Math.cos(theta));
}

function sini(theta) {
    return Math.round(Math.sin(theta));
}

function tr(z) {
    if (z > 0.5)
        z -= 0.5;
    var f = 1;
    return 0.5 - .5 * f / (f + z);
}

function drawLeft(t, u, isWall, isEnd) {
    if (isWall) {
        drawLine(t, t, u, u);
        if(isEnd) drawLine(u, u, u, 1 - u);
        drawLine(u, 1 - u, t, 1 - t);
    } else {
        drawLine(t, u, u, u);
        if(!isEnd) drawLine(u, u, u, 1 - u);
        if(t > 0) drawLine(t, t, t, 1 - t);
        drawLine(u, 1 - u, t, 1 - u);
    }
}

function drawRight(t, u, isWall, isEnd) {
    if (isWall) {
        drawLine(1 - t, t, 1 - u, u);
        if(isEnd) drawLine(1 - u, u, 1 - u, 1 - u);
        drawLine(1 - u, 1 - u, 1 - t, 1 - t);
    } else {
        drawLine(1 - t, u, 1 - u, u);
        if(!isEnd) drawLine(1 - u, u, 1 - u, 1 - u);
        if(t > 0) drawLine(1 - t, t, 1 - t, 1 - t);
        drawLine(1 - u, 1 - u, 1 - t, 1 - u);
    }
    if(isEnd) {
        drawLine(u, u, 1 - u, u);
        drawLine(u, 1 - u, 1 - u, 1 - u);
    }
}

function drawEnd(t) {
}

function drawKey(z) {
    var n = 15;
    var j = (n + 1) * Math.PI / 2 / n;
    var l = .5 / (1 + z);

    for (var i = 0; i < n * j; i += j) {
        drawLine(0.5 + l * Math.cos(i), (0.5 + l * Math.sin(i)),
                0.5 + l * Math.cos(i + j), (0.5 + l * Math.sin(i + j)));
    }
}

function drawLine(x0, y0, x1, y1) {
    g.beginPath();
    g.moveTo(BW * x0, BW * y0);
    g.lineTo(BW * x1, BW * y1);
    g.stroke();
}

// random number from [from] (inclusive) to [to] (exclusive)
function randInt(from, to) {
    return from + Math.floor((to - from) * Math.random());
}
